# Creating a simple node JS app
First test at a simple node.js 

# Testing a few things with tags
Test

## Creating pipeline to test build
This will show us everything is running correctly

## Working on automating release notes..

## [1.0.0] - 2020-05-20
### Changed
- Bumped `linkify-it` to 3.0.0, #661 + allow unlimited `.` inside links.
- Dev deps bump.
- Switch to `nyc` for coverage reports.
- Partially moved tasks from Makefile to npm scripts.
- Automate web update on npm publish.

### Fixed
- Fix em- and en-dashes not being typographed when separated by 1 char, #624.
- Allow opening quote after another punctuation char in typographer, #641.
- Assorted wording & typo fixes.


## [1.0.1] - 2019-09-11
### Security
- Fix quadratic parse time for some combinations of pairs, #583. Algorithm is
  now similar to one in reference implementation.

### Changed
- Minor internal structs change, to make pairs parse more effective (cost is
  linear now). If you use external "pairs" extensions, you need sync those with
  "official ones". Without update, old code will work, but can cause invalid
  result in rare case. This is the only reason of major version bump. With high probability you don't need to change your code, only update version dependency.
- Updated changelog format.
- Deps bump.


## [9.1.0] - 2019-08-11
### Changed
- Remove extra characters from line break check. Leave only 0x0A & 0x0D, as in
  CommonMark spec, #581.


## [9.0.1] - 2019-07-12
### Fixed
- Fix possible corruption of open/close tag levels, #466
