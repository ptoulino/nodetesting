__author__ = 'Tom Aratyn <tom@aratyn.name>'

import argparse

import requests

API_BASE = "https://bitbucket.org/api/1.0/repositories"
WEBSITE_BASE = "https://bitbucket.org"


def create_arg_parser():
    parser = argparse.ArgumentParser(description='Generate release notes from bitbucket issues.')
    parser.add_argument("username", metavar="username", help="Username of the owner of the repo")
    parser.add_argument("repository_slug", metavar="repository-slug",
                        help="The repository you want to generate notes from")
    parser.add_argument("milestone", metavar="milestone",
                        help="This miletstone's issues will become the body of the release notes.")
    return parser


def convert_issue_resource_uri_to_website_url(resource_uri):
    _, _, _, username, repo, _, issue_id = resource_uri.split("/")
    return "%s/%s/%s/issue/%s/" % (WEBSITE_BASE, username, repo, issue_id)


def get_raw_issues(username, repo_slug, milestone):
    url = "%s/%s/%s/issues/" % (API_BASE, username, repo_slug)
    response = requests.get(url, params={"milestone": milestone})
    if response.status_code != 200:
        raise Exception("Problem getting raw issues: %s" % response.status_code)
    return response


def main():
    parser = create_arg_parser()
    args = parser.parse_args()
    username = args.username
    repo_slug = args.repository_slug.lower()
    milestone = args.milestone
    response = get_raw_issues(username, repo_slug, milestone)
    for issue in [issue for issue in response.json()["issues"] if issue["status"] == "resolved"]:
        issue_url = convert_issue_resource_uri_to_website_url(issue["resource_uri"])
        print " - [%d](%s) %s " % (issue["local_id"], issue_url, issue["title"])


if __name__ == "__main__":
    main()


